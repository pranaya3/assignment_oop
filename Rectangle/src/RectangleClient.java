public class RectangleClient {
    public static void main(String[] args){
        Rectangle rectangle1 = new Rectangle(5,50,"yellow");
        // Display the width, height, area, and perimeter of rectangle1
        System.out.println("\n Rectangle 1");
        System.out.println("-------------");
        System.out.println("Width:     " + rectangle1.getWidth());
        System.out.println("Height:    " + rectangle1.getHeight());
        System.out.println("Color:    " + rectangle1.getColor());
        System.out.println("Area:      " + rectangle1.getArea());
        System.out.println("Perimeter: " + rectangle1.getPerimeter());

        Rectangle rectangle2 = new Rectangle(10,10,"black");
        // Display the width, height, area, and perimeter of rectangle2
        System.out.println("\n Rectangle 2");
        System.out.println("-------------");
        System.out.println("Width:     " + rectangle2.getWidth());
        System.out.println("Height:    " + rectangle2.getHeight());
        System.out.println("Color:    " + rectangle2.getColor());
        System.out.println("Area:      " + rectangle2.getArea());
        System.out.println("Perimeter: " + rectangle2.getPerimeter());
    }
}
