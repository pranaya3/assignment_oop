public class PayrollDriver {
    public static void main(String[] args){

        Payroll emp1 = new Payroll("Pranaya",1);

        emp1.setHoursWorked(70.5);
        emp1.setPayRate(8.20);

        System.out.println("Payroll info:"+"\n"+
                "Name:" +emp1.getName()+"\n"+
                "Id Number:" +emp1.getIdNumber()+ "\n"+
                "Hours Worked:" +emp1.getHoursWorked() + "\n"+
                "Pay Rate:" +emp1.getPayRate() + "\n"+
                "Gross Pay:" +emp1.getGrossPay());
    }
}
