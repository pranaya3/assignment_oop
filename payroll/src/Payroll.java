public class Payroll {
    //Instance Field
    private  String name;
    private int idNumber;
    private double payRate;
    private double hoursWorked;
    //constructor
    public Payroll(String n, int i){
        this.name = n;
        this.idNumber = i;
    }

    //mutators
    public void setName(String n)
    {
        this.name = n;
    }
    public void setIdNumber(int i){
        this.idNumber = i;
    }

    public void setPayRate(double p)
    {
        this.payRate = p;
    }

    public void setHoursWorked(double h)
    {
        this.hoursWorked=h;
    }

    //Accessor Method
    public String getName(){
        return this.name;
    }
    public int getIdNumber(){
        return this.idNumber;
    }

    public double getHoursWorked() {
        return this.hoursWorked;
    }

    public double getPayRate() {
        return this.payRate;
    }

    public double getGrossPay(){
        return hoursWorked * payRate;
    }


}
