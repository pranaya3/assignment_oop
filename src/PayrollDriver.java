public class PayrollDriver {
    public static void main(String[] args){
        PayRoll emp1 = new PayRoll("Pranaya",1);
        emp1.setHoursWorked(70.5);
        emp1.setPayRate(8.20);

        System.out.println("Payroll Info" + "\n" +
                            "Name: " +emp1.getName() +"\n"+
                            "IdNumber: " +emp1.getIdNumber() +"\n" +
                            "Hours Worked: " +emp1.getHoursWorked() + "\n"+
                            "Pay Rate: " +emp1.getPayRate() +"\n"+
                            "Gross Pay: " +emp1.getGrossPay() +"\n");
    }
}
