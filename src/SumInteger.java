/*      Write a program that reads an integer between 0 and 1000 and
        adds all the digits in the integer.
        For example, if an integer is 943,
        the sum of all its digit is 16.
        */

import java.util.Scanner;
public class SumInteger {
    public static void main(String[] args){
        int number, sum =0, sum1 = 0;
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the number between 0 and 1000");
        number = sc.nextInt();
        while(number >0) {
            if (number < 0 || number >= 1000) {
                System.out.println("Enter the number between 0 and 1000");
                number = sc.nextInt();
            }
            else {

               while (number > 0) {
                    sum += number % 10; // Add the last digit

                    number /= 10;       // Remove the last digit
                }


              /* sum = sum + (number % 10);
                number = number / 10;

                sum = sum + (number % 10);
                number = number / 10;

                sum = sum + (number % 10);
                number = number / 10;*/

                System.out.println("Sum:" + sum);
                System.out.println("Number:" +number);


            }
        }
        sc.close();


    }
}
