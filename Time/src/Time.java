import java.util.Date;

public class Time {
    private int hour;
    private int minute;
    private int second;

    // No-arg constructor to set the current time in GMT
    public Time() {
        setTime(System.currentTimeMillis());
    }

    // Constructor to set a specific time in GMT
    public Time(long elapsedTime) {
        setTime(elapsedTime);
    }

    // Set the time based on the elapsed time
    public void setTime(long elapsedTime) {
        // Obtain the total seconds since the epoch
        long totalSeconds = elapsedTime / 1000;

        // Calculate the current second
        second = (int) (totalSeconds % 60);

        // Obtain the total minutes
        long totalMinutes = totalSeconds / 60;

        // Calculate the current minute
        minute = (int) (totalMinutes % 60);

        // Obtain the total hours
        long totalHours = totalMinutes / 60;

        // Calculate the current hour
        hour = (int) (totalHours % 24);
    }

    // Getter method for hour
    public int getHour() {
        return hour;
    }

    // Getter method for minute
    public int getMinute() {
        return minute;
    }

    // Getter method for second
    public int getSecond() {
        return second;
    }

    public static void main(String[] args) {
        // Create a Time object with the current time
        Time currentTime = new Time();

        // Display the current hour, minute, and second
        System.out.println("Current Time (GMT):");
        System.out.println("Hour: " + currentTime.getHour());
        System.out.println("Minute: " + currentTime.getMinute());
        System.out.println("Second: " + currentTime.getSecond());
    }
}
