import java.util.TimeZone;

public class TimeNepal {
    private int hour;
    private int minute;
    private int second;

    // No-arg constructor to set the current time in GMT+5:45 (Nepal Standard Time)
    public TimeNepal() {
        setTime(System.currentTimeMillis());
    }

    // Constructor to set a specific time in GMT+5:45 (Nepal Standard Time)
    public TimeNepal(long elapsedTime) {
        setTime(elapsedTime);
    }

    // Set the time based on the elapsed time
    public void setTime(long elapsedTime) {
        // Set the time zone to Nepal Standard Time (GMT+5:45)
        TimeZone timeZone = TimeZone.getTimeZone("Asia/Kathmandu");

        // Set the time zone for the current thread
        TimeZone.setDefault(timeZone);

        // Obtain the total seconds since the epoch
        long totalSeconds = elapsedTime / 1000;

        // Calculate the current second
        second = (int) (totalSeconds % 60);

        // Obtain the total minutes
        long totalMinutes = totalSeconds / 60;

        // Calculate the current minute
        minute = (int) (totalMinutes % 60);

        // Obtain the total hours
        long totalHours = totalMinutes / 60;

        // Calculate the current hour
        hour = (int) (totalHours % 24);
    }

    // Getter method for hour
    public int getHour() {
        return hour;
    }

    // Getter method for minute
    public int getMinute() {
        return minute;
    }

    // Getter method for second
    public int getSecond() {
        return second;
    }

    public static void main(String[] args) {
        // Create a Time object with the current time in Nepal
        Time nepalTime = new Time();

        // Display the current hour, minute, and second
        System.out.println("Current Time in Nepal (GMT+5:45):");
        System.out.println("Hour: " + nepalTime.getHour());
        System.out.println("Minute: " + nepalTime.getMinute());
        System.out.println("Second: " + nepalTime.getSecond());
    }
}
