import java.util.GregorianCalendar;
public class Main {


           /** Main method */
        public static void main(String[] args) {
           // Create a GregorianCalendar instance for the current date
                GregorianCalendar currentDate = new GregorianCalendar();

                // Display current year, month, and day
                System.out.println("Current Date:");
                displayDate(currentDate);
            }

            private static void displayDate(GregorianCalendar date) {
                int year = date.get(GregorianCalendar.YEAR);
                int month = date.get(GregorianCalendar.MONTH) + 1; // Adding 1 since months are zero-based
                int day = date.get(GregorianCalendar.DAY_OF_MONTH);

                System.out.println("Year: " + year);
                System.out.println("Month: " + month);
                System.out.println("Day: " + day);
            }
}